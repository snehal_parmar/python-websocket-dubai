# README #

Following are the steps to start application:

1) Start redis server on localhost using following command: 
   # redis-server

2) Change directory into python-websocket-dubai source directory:
   # cd python-websocket-dubai

3) Start the application :
   # python app.py

4) Finally point the browser to following url:
   #http://localhost:5000/top_25_topic_Dubai/

Thanks