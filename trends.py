#!/usr/bin/python

#-----------------------------------------------------------------------
# twitter-trends
#  - lists the current global trending topics
#-----------------------------------------------------------------------
import tweepy
import redis
# import Thread 


def get_tweets(woeid):
    #-----------------------------------------------------------------------
    # load our API credentials 
    #-----------------------------------------------------------------------
    config = {}
    execfile("config_twitter.py", config)

    #-----------------------------------------------------------------------
    # create twitter API object
    #-----------------------------------------------------------------------
    auth = tweepy.OAuthHandler(config["consumer_key"], config["consumer_secret"])
    auth.set_access_token(config["access_key"], config["access_secret"])
    api = tweepy.API(auth)

    #-----------------------------------------------------------------------
    # retrieve Dubai trends.
    # other localised trends can be specified by looking up WOE IDs:
    #   http://developer.yahoo.com/geo/geoplanet/
    # twitter API docs: https://dev.twitter.com/docs/api/1/get/trends/%3Awoeid
    #-----------------------------------------------------------------------
    # woeid_Dubai = 1940345
    flag = True
    names = []
    while(flag):
        trends1 = api.trends_place(woeid)
        data = trends1[0]
        trends = data['trends']
        names_all = [trend['name'] for trend in trends]
        names = names_all + names    
        #import pdb; pdb.set_trace()    
        if len(names) >= 25:
            flag = False

    names_top_25 = names[:25]
    return names_top_25


class StreamListener(tweepy.StreamListener):
    #count = 0
    def on_status(self, status):
        # c = db.incr('count')
        # import pdb; pdb.set_trace()
        db = redis.StrictRedis('localhost', 6379, 0)
        db.lpush('tweet_list', status.text)
        if db.llen('tweet_list') > 20:
            db.ltrim('tweet_list', 0, -10)
        # print status.text
        # print db.lrange('tweet_list', 0, 25)
        

    def on_error(self, status_code):
        if status_code == 420:
            return False


def get_more_tweets(topic_index):
    try:
            #-----------------------------------------------------------------------
        # load our API credentials 
        #-----------------------------------------------------------------------
        config = {}
        execfile("config_twitter.py", config)
        # db = redis.StrictRedis('localhost', 6379, 0)
        #-----------------------------------------------------------------------
        # create twitter API object
        #-----------------------------------------------------------------------
        auth = tweepy.OAuthHandler(config["consumer_key"], config["consumer_secret"])
        auth.set_access_token(config["access_key"], config["access_secret"])
        api = tweepy.API(auth)
        stream_listener = StreamListener()
        stream = tweepy.Stream(auth=api.auth, listener=stream_listener)
        topics_list = get_tweets(1940345)
        topic = topics_list[topic_index]
        # import pdb; pdb.set_trace()
        stream.filter(track=topic)
    except Exception as e:
        print e
        # return False



if __name__ == '__main__':
    #-----------------------------------------------------------------------
    # load our API credentials 
    #-----------------------------------------------------------------------
    config = {}
    execfile("config_twitter.py", config)
    db = redis.StrictRedis('localhost', 6379, 0)

    #-----------------------------------------------------------------------
    # create twitter API object
    #-----------------------------------------------------------------------
    auth = tweepy.OAuthHandler(config["consumer_key"], config["consumer_secret"])
    auth.set_access_token(config["access_key"], config["access_secret"])
    api = tweepy.API(auth)

    #-----------------------------------------------------------------------
    # retrieve Dubai trends.
    # other localised trends can be specified by looking up WOE IDs:
    #   http://developer.yahoo.com/geo/geoplanet/
    # twitter API docs: https://dev.twitter.com/docs/api/1/get/trends/%3Awoeid
    #-----------------------------------------------------------------------
    flag = True
    names = []
    while(flag):
        trends1 = api.trends_place(1940345)
        data = trends1[0]
        trends = data['trends']
        names_all = [trend['name'] for trend in trends]
        names = names_all + names    
        if len(names) >= 25:
            flag = False

    names_top_25 = names[:25]
    

    # get_more_tweets()
    topics_list = names_top_25
    test_list = get_more_tweets()
    print test_list