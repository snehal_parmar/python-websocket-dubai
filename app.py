from gevent import monkey
monkey.patch_all()

import cgi
import redis
from flask import Flask, render_template, request
from flask_socketio import SocketIO
from trends import get_tweets, get_more_tweets
import threading
import time


app = Flask(__name__)
db = redis.StrictRedis('localhost', 6379, 0)
socketio = SocketIO(app)


@app.route('/')
def main():
    return render_template('main.html')


@app.route('/top_25_topic_Dubai/')
def top_25_topic_Dubai():
    woeid_Dubai = 1940345
    topic_list = get_tweets(woeid_Dubai)
    return render_template('top_25_topic_Dubai.html', name=topic_list)


# as socket is there we can have bidirectional communiation between client and server
# 
# @app.route('/get_tweet_topic/')
# def get_tweet_topic():
#     # topic_list = get_tweets()
#     print "api for fetching more tweets for topic"
#     #import pdb; pdb.set_trace()
#     # t = threading.Thread(target=get_more_tweets)
#     # t.start()
#     ret_res = get_more_tweets()
#     if ret_res:
#         return "True"
#     else:
#         return "False"

# @socketio.on('connect', namespace='/dd')
# def ws_conn():
#     c = db.incr('connected')
#     socketio.emit('msg', {'count': c}, namespace='/dd')


# @socketio.on('disconnect', namespace='/dd')
# def ws_disconn():
#     c = db.decr('connected')
#     socketio.emit('msg', {'count': c}, namespace='/dd')

@socketio.on('tweet', namespace='/dd')
def ws_tweet(message):
    # print message
    # print "**************"
    
    topic_index = message['topic_index']
    t = threading.Thread(target=get_more_tweets, args=(int(topic_index),))
    t.start()
    time.sleep(5)
    tweet_list = db.lrange('tweet_list', 0, 20)
    # print tweet_list
    # import pdb; pdb.set_trace()
    # woeid_Dubai = 1940345
    # lt = get_tweets(woeid_Dubai)
    for x in tweet_list:
        print x
    socketio.emit('tweet', {'tweet': tweet_list}, namespace="/dd")

if __name__ == '__main__':
    db.delete('tweet_list')
    socketio.run(app, port=5000)
